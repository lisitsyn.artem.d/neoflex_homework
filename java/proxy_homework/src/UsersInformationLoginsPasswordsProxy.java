public class UsersInformationLoginsPasswordsProxy implements UsersInformation {
    private UsersInformationLoginsPasswords loginsPasswords = new UsersInformationLoginsPasswords();

    private static String[] loginsPasswordsCache;

    @Override
    public String[] getUsersLoginsPasswords() {
        if (loginsPasswordsCache == null) {
            loginsPasswordsCache = loginsPasswords.getUsersLoginsPasswords();
        }
        return loginsPasswordsCache;
    }
}
