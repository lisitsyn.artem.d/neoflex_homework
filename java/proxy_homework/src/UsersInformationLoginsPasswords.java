import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UsersInformationLoginsPasswords implements UsersInformation{
    @Override
    public String[] getUsersLoginsPasswords() {
        List<String> list = new ArrayList<>();
        try(Scanner scanner = new Scanner(new FileReader("./info.csv"))) {
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();
                list.add(line);
            }
        }
        catch (IOException e){
            System.out.println("Error: " + e);
        }
        return list.toArray(new String[list.size()]);
    }
}
