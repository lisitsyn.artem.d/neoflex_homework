public class DisplayUsersLoginsPasswords {
    private UsersInformation usersInformation = new UsersInformationLoginsPasswordsProxy();

    public void printLoginsPasswords() {
        String[] loginsPasswords = usersInformation.getUsersLoginsPasswords();
        String[] temp;

        System.out.println("Login\t\tPassword");
        for (int i=0;i<loginsPasswords.length;i++){
            temp = loginsPasswords[i].split(";");
            System.out.printf("%s\t\t%s\n",temp[0],temp[1]);
        }
    }
}
