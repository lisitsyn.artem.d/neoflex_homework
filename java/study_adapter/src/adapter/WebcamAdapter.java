package adapter;

public class WebcamAdapter implements USB {
    private Webcam webcam;

    public WebcamAdapter(Webcam webcam) {
        setWebcam(webcam);
    }

    public void setWebcam(Webcam webcam) {
        this.webcam = webcam;
    }

    public Webcam getWebcam() {
        return webcam;
    }

    public Webcam removeWebcam() {
        Webcam webc = this.webcam;
        this.webcam = null;
        return webc;
    }

    @Override
    public void connectWithUsbCable() {
        getWebcam().connect();
        getWebcam().takePhoto();
    }

    @Override
    public void disconnectWithUsbCable() {
        getWebcam().disconnect();
    }
}
