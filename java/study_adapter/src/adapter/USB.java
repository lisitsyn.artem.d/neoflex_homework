package adapter;

public interface USB {

    void connectWithUsbCable();
    void disconnectWithUsbCable();
}
