package adapter;

public class Webcam {
    public void connect() {
        System.out.println("Веб камера успшено подключена!");
    }

    public void takePhoto() {
        System.out.println("Фото сделано.");
    }

    public void disconnect() {
        System.out.println("Веб камера успешно отключена");
    }
}
