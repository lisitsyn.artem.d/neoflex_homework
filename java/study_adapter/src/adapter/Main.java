package adapter;

public class Main {

    public static void main(String[] args) {
        USB cardReader = new CardReader(new MicroSD());
        cardReader.connectWithUsbCable();
        cardReader.disconnectWithUsbCable();

        USB webcamAdapter = new WebcamAdapter(new Webcam());
        webcamAdapter.connectWithUsbCable();
    }
}
