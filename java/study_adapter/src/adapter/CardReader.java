package adapter;

public class CardReader implements USB {

    private MicroSD microSD;

    public CardReader(MicroSD microSD) {
        setMicroSD(microSD);
    }

    @Override
    public void disconnectWithUsbCable() {
        removeMicroSD().disconnect();
    }

    @Override
    public void connectWithUsbCable() {
        getMicroSD().insert();
        getMicroSD().copyData();
    }

    public MicroSD getMicroSD() {
        return microSD;
    }

    public void setMicroSD(MicroSD microSD) {
        this.microSD = microSD;
    }

    public MicroSD removeMicroSD() {
        MicroSD micSD = this.microSD;
        this.microSD = null;
        return micSD;
    }
}
